package com.marsrover.planet;

import com.marsrover.position.Coordinates;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for Obstacle class
 *
 * @author Adrián Martín Sánchez
 */
public class ObstacleTest {

    private Obstacle obstacle;
    private Coordinates coordinates;

    private final static Integer coordinateX = 10;
    private final static Integer coordinateY = 15;

    @Before
    public void setup() throws Exception {
        coordinates = new Coordinates(coordinateX, coordinateY);
        obstacle    = new Obstacle(coordinates);
    }

    @Test
    public void obstacle_WithNullArgument() throws Exception {
        try {
            Obstacle obstacle = new Obstacle(null);
            assertTrue("No exception raised", false);
        }
        catch (Exception exception){
            assertTrue(true);
        }
    }

    @Test
    public void equals() throws Exception {
        Obstacle obstacle2 = new Obstacle(new Coordinates(coordinateX, coordinateY));

        assertEquals(obstacle, obstacle2);
    }

    @Test
    public void notEquals() throws Exception {
        Obstacle obstacle2 = new Obstacle(new Coordinates(coordinateX + 1, coordinateY));

        assertNotEquals(obstacle, obstacle2);
    }
}