package com.marsrover.planet;

import com.marsrover.position.Coordinates;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Unit test for Planet class
 *
 * @author Adrián Martín Sánchez
 */
public class PlanetTest {

    private Planet planet;
    private Set<Obstacle> obstacles = new HashSet<>();

    private static final Integer sizeX                      = 4;
    private static final Integer sizeY                      = 4;
    private static final Integer coordinate1X               = 0;
    private static final Integer coordinate1Y               = 0;
    private static final Integer coordinate2X               = 0;
    private static final Integer coordinate2Y               = 1;
    private static final Integer coordinateOut1X            = 7;
    private static final Integer coordinateOut1Y            = 5;
    private static final Integer coordinateOut2X            = -1;
    private static final Integer coordinateOut2Y            = -5;


    @Test
    public void planet_WithNegativeSizeX() throws Exception {
        try {
            planet = new Planet.Builder()
                    .sizeX(-1)
                    .sizeY(2)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception){
            assertTrue(exception.getMessage(), true);
        }
    }

    @Test
    public void planet_WithNegativeSizeY() throws Exception {
        try {
            planet = new Planet.Builder()
                    .sizeX(2)
                    .sizeY(-1)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception){
            assertTrue(exception.getMessage(), true);
        }
    }

    @Test
    public void planet_WithOutSizeX() throws Exception {
        try {
            planet = new Planet.Builder()
                    .sizeY(2)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception){
            assertTrue(exception.getMessage(), true);
        }
    }

    @Test
    public void planet_WithoutSizeY() throws Exception {
        try {
            planet = new Planet.Builder()
                    .sizeX(2)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception){
            assertTrue(exception.getMessage(), true);
        }
    }

    @Test
    public void initialize_withoutObstacles() throws Exception {
        Integer [][] mapExpected = {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};

        planet = Mockito.spy(new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .build());

        planet.initialize();

        Mockito.verify(planet, Mockito.times(0)).calculateNextPosition(Mockito.any());

        for (int i = 0; i < mapExpected.length; i++) {
            assertArrayEquals(mapExpected[i], planet.getMap()[i]);
        }
    }

    @Test
    public void initialize_withObstacles() throws Exception {
        Integer [][] mapExpected = {{1, 0, 0, 0}, {1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};

        obstacles.add(new Obstacle(new Coordinates(coordinate1X, coordinate1Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinate2X, coordinate2Y)));

        planet = Mockito.spy(new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .obstacles(obstacles)
                .build());

        planet.initialize();

        Mockito.verify(planet, Mockito.times(2)).calculateNextPosition(Mockito.any());

        for (int i = 0; i < mapExpected.length; i++) {
            assertArrayEquals(mapExpected[i], planet.getMap()[i]);
        }
    }

    @Test
    public void initialize_withObstaclesOutOffLimits() throws Exception {
        Integer [][] mapExpected = {{0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 0, 0}, {0, 0, 0, 1}};

        obstacles.add(new Obstacle(new Coordinates(coordinateOut1X, coordinateOut1Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinateOut2X, coordinateOut2Y)));

        planet = Mockito.spy(new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .obstacles(obstacles)
                .build());

        planet.initialize();

        Mockito.verify(planet, Mockito.times(2)).calculateNextPosition(Mockito.any());

        for (int i = 0; i < mapExpected.length; i++) {
            assertArrayEquals(mapExpected[i], planet.getMap()[i]);
        }

    }

    @Test
    public void initialize_withObstaclesRepeated() throws Exception {
        Integer [][] mapExpected = {{1, 0, 0, 0}, {1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};

        obstacles.add(new Obstacle(new Coordinates(coordinate1X, coordinate1Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinate2X, coordinate2Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinate1X, coordinate1Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinate2X, coordinate2Y)));

        planet = Mockito.spy(new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .obstacles(obstacles)
                .build());

        planet.initialize();

        Mockito.verify(planet, Mockito.times(2)).calculateNextPosition(Mockito.any());

        for (int i = 0; i < mapExpected.length; i++) {
            assertArrayEquals(mapExpected[i], planet.getMap()[i]);
        }
    }

    @Test
    public void isPositionOccupied_Available() throws Exception {
        Coordinates positionAvailableExpected = new Coordinates(1, 0);

        obstacles.add(new Obstacle(new Coordinates(coordinate1X, coordinate1Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinate2X, coordinate2Y)));

        planet = new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .obstacles(obstacles)
                .build();

        planet.initialize();

        assertTrue(planet.isPositionAvailable(positionAvailableExpected));
    }

    @Test
    public void isPositionOccupied_Occupied() throws Exception {
        Coordinates positionOccupiedExpected = new Coordinates(coordinate1X, coordinate1Y);

        obstacles.add(new Obstacle(new Coordinates(coordinate1X, coordinate1Y)));
        obstacles.add(new Obstacle(new Coordinates(coordinate2X, coordinate2Y)));

        planet = new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .obstacles(obstacles)
                .build();

        planet.initialize();

        assertFalse(planet.isPositionAvailable(positionOccupiedExpected));
    }

    @Test
    public void calculateNextPosition_WithinLimits() throws Exception {
        Coordinates positionWithinLimits = new Coordinates(coordinate1X, coordinate1Y);

        planet = new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .build();

        assertEquals(positionWithinLimits, planet.calculateNextPosition(positionWithinLimits));
    }

    @Test
    public void calculateNextPosition_OutOffLimits() throws Exception {
        Coordinates positionOutOffLimits    = new Coordinates(coordinateOut1X, coordinateOut2Y);
        Coordinates positionExpected        = new Coordinates(3, 3);

        planet = new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .build();

        assertEquals(positionExpected, planet.calculateNextPosition(positionOutOffLimits));
    }
}