package com.marsrover.robot;

import com.marsrover.planet.Obstacle;
import com.marsrover.planet.Planet;
import com.marsrover.position.Coordinates;
import com.marsrover.position.Direction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * Unit test for Rover class
 *
 * @author Adrián Martín Sánchez
 */
public class RoverTest {

    private Rover roverSpy;
    private Planet planetSpy;

    private static final Integer sizeX              = 2;
    private static final Integer sizeY              = 2;
    private static final Integer coordinateRoverX   = 0;
    private static final Integer coordinateRoverY   = 0;
    private static final Direction direction        = Direction.N;

    private Coordinates moveForward_FromDirection(Direction initialDirection) throws Exception {
        ArgumentCaptor<Coordinates> coordinatesCaptor = ArgumentCaptor.forClass(Coordinates.class);
        Mockito.doNothing().when(roverSpy).moveRobot(Mockito.any());

        roverSpy.direction = initialDirection;
        roverSpy.moveForward();

        Mockito.verify(roverSpy).moveRobot(coordinatesCaptor.capture());

        return coordinatesCaptor.getValue();
    }

    private Coordinates moveBackward_FromDirection(Direction initialDirection) throws Exception {
        Mockito.doNothing().when(roverSpy).moveRobot(Mockito.any());
        ArgumentCaptor<Coordinates> coordinatesCaptor = ArgumentCaptor.forClass(Coordinates.class);

        roverSpy.direction = initialDirection;
        roverSpy.moveBackward();

        Mockito.verify(roverSpy).moveRobot(coordinatesCaptor.capture());

        return coordinatesCaptor.getValue();
    }

    @Before
    public void setup() throws Exception {
        planetSpy = Mockito.spy(new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .build());

        roverSpy = Mockito.spy(new Rover.Builder()
                .direction(direction)
                .position(new Coordinates(coordinateRoverX, coordinateRoverY))
                .planet(planetSpy)
                .build());
    }

    @Test
    public void rover_WithoutDirection() throws Exception {
        try {
            Rover rover = new Rover.Builder()
                    .position(new Coordinates(coordinateRoverX, coordinateRoverY))
                    .planet(planetSpy)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void rover_WithoutPosition() throws Exception {
        try {
            Rover rover = new Rover.Builder()
                    .direction(direction)
                    .planet(planetSpy)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void rover_WithoutPlanet() throws Exception {
        try {
            Rover rover = new Rover.Builder()
                    .position(new Coordinates(coordinateRoverX, coordinateRoverY))
                    .direction(direction)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void rover_WithStartPositionNegative() throws Exception {
        Coordinates positionExpected = new Coordinates(coordinateRoverX, coordinateRoverY);

        Rover rover = new Rover.Builder()
                .position(new Coordinates(-sizeX, -sizeY))
                .direction(direction)
                .planet(planetSpy)
                .build();

        assertEquals(positionExpected, rover.getPosition());
    }

    @Test
    public void rover_WithStartPositionOccupied() throws Exception {
        try {
            Rover.Builder roverBuilderSpy = Mockito.spy(new Rover.Builder());
            Mockito.doReturn(false).when(planetSpy).isPositionAvailable(Mockito.any());

            Rover rover = roverBuilderSpy
                    .position(new Coordinates(coordinateRoverX, coordinateRoverY))
                    .direction(direction)
                    .planet(planetSpy)
                    .build();

            assertTrue("No exception raised", false);
        }
        catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void runAllCommands() throws Exception {
        Mockito.doNothing().when(roverSpy).moveForward();
        Mockito.doNothing().when(roverSpy).moveBackward();
        Mockito.doNothing().when(roverSpy).turnLeft();
        Mockito.doNothing().when(roverSpy).turnRight();

        roverSpy.setInputCommands(new char [] {'f', 'b', 'l', 'r'});
        roverSpy.runAllCommands();

        Mockito.verify(roverSpy, Mockito.times(1)).moveForward();
        Mockito.verify(roverSpy, Mockito.times(1)).moveBackward();
        Mockito.verify(roverSpy, Mockito.times(1)).turnLeft();
        Mockito.verify(roverSpy, Mockito.times(1)).turnRight();
    }



    @Test
    public void isPossibleToMove_Available() throws Exception {
        Mockito.doReturn(true).when(roverSpy.planet).isPositionAvailable(Mockito.any());

        Coordinates positionAvailableExpected = new Coordinates(coordinateRoverX, coordinateRoverY);

        assertTrue(roverSpy.isPossibleToMove(positionAvailableExpected));
    }

    @Test
    public void isPossibleToMove_Occupied() throws Exception {
        Mockito.doReturn(false).when(roverSpy.planet).isPositionAvailable(Mockito.any());

        Coordinates positionOccupiedExpected = new Coordinates(coordinateRoverX, coordinateRoverY);

        assertFalse(roverSpy.isPossibleToMove(positionOccupiedExpected));
    }

    @Test
    public void moveForward() throws Exception {
        Mockito.doNothing().when(roverSpy).moveRobot(Mockito.any());

        roverSpy.moveForward();

        Mockito.verify(roverSpy, Mockito.times(1)).moveRobot(Mockito.any());
    }

    @Test
    public void moveForward_FromN() throws Exception {
        Coordinates positionExpected = new Coordinates(0, 1);

        assertEquals(positionExpected, moveForward_FromDirection(Direction.N));
    }

    @Test
    public void moveForward_FromS() throws Exception {
        Coordinates positionExpected = new Coordinates(0, -1);

        assertEquals(positionExpected, moveForward_FromDirection(Direction.S));
    }

    @Test
    public void moveForward_FromE() throws Exception {
        Coordinates positionExpected = new Coordinates(1, 0);

        assertEquals(positionExpected, moveForward_FromDirection(Direction.E));
    }

    @Test
    public void moveForward_FromW() throws Exception {
        Coordinates positionExpected = new Coordinates(-1, 0);

        assertEquals(positionExpected, moveForward_FromDirection(Direction.W));
    }

    @Test
    public void moveBackward() throws Exception {
        Mockito.doNothing().when(roverSpy).moveRobot(Mockito.any());

        roverSpy.moveBackward();

        Mockito.verify(roverSpy, Mockito.times(1)).moveRobot(Mockito.any());
    }

    @Test
    public void moveBackward_FromN() throws Exception {
        Coordinates positionExpected = new Coordinates(0, -1);

        assertEquals(positionExpected, moveBackward_FromDirection(Direction.N));
    }

    @Test
    public void moveBackward_FromS() throws Exception {
        Coordinates positionExpected = new Coordinates(0, 1);

        assertEquals(positionExpected, moveBackward_FromDirection(Direction.S));
    }

    @Test
    public void moveBackward_FromE() throws Exception {
        Coordinates positionExpected = new Coordinates(-1, 0);

        assertEquals(positionExpected, moveBackward_FromDirection(Direction.E));
    }

    @Test
    public void moveBackward_FromW() throws Exception {
        Coordinates positionExpected    = new Coordinates(1, 0);

        assertEquals(positionExpected, moveBackward_FromDirection(Direction.W));
    }

    @Test
    public void turnLeft_FromN() throws Exception {
        Direction directionExpected = Direction.W;

        roverSpy.direction = Direction.N;
        roverSpy.turnLeft();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnLeft_FromS() throws Exception {
        Direction directionExpected = Direction.E;

        roverSpy.direction = Direction.S;
        roverSpy.turnLeft();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnLeft_FromE() throws Exception {
        Direction directionExpected = Direction.N;

        roverSpy.direction = Direction.E;
        roverSpy.turnLeft();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnLeft_FromW() throws Exception {
        Direction directionExpected = Direction.S;

        roverSpy.direction = Direction.W;
        roverSpy.turnLeft();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnRight_FromN() throws Exception {
        Direction directionExpected = Direction.E;

        roverSpy.direction = Direction.N;
        roverSpy.turnRight();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnRight_FromS() throws Exception {
        Direction directionExpected = Direction.W;

        roverSpy.direction = Direction.S;
        roverSpy.turnRight();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnRight_FromE() throws Exception {
        Direction directionExpected = Direction.S;

        roverSpy.direction = Direction.E;
        roverSpy.turnRight();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void turnRight_FromW() throws Exception {
        Direction directionExpected = Direction.N;

        roverSpy.direction = Direction.W;
        roverSpy.turnRight();

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void moveRobot_IsPossibleToMove() throws Exception {
        Coordinates coordinatesExpected = new Coordinates(0, 1);

        Mockito.doReturn(coordinatesExpected).when(planetSpy).calculateNextPosition(Mockito.any());
        Mockito.doReturn(true).when(roverSpy).isPossibleToMove(Mockito.any());

        roverSpy.moveRobot(coordinatesExpected);

        assertEquals(coordinatesExpected, roverSpy.getPosition());
        assertEquals(0, roverSpy.obstaclesFound.size());
    }

    @Test
    public void moveRobot_IsNotPossibleToMove() throws Exception {
        Coordinates coordinatesNotExpected = new Coordinates(0, 1);

        Mockito.doReturn(coordinatesNotExpected).when(planetSpy).calculateNextPosition(Mockito.any());
        Mockito.doReturn(false).when(roverSpy).isPossibleToMove(Mockito.any());

        roverSpy.moveRobot(coordinatesNotExpected);

        assertEquals(new Coordinates(coordinateRoverX, coordinateRoverY), roverSpy.getPosition());
        assertEquals(1, roverSpy.obstaclesFound.size());
        assertTrue(roverSpy.obstaclesFound.contains(new Obstacle(coordinatesNotExpected)));
    }

    @Test
    public void turnRobot() throws Exception {
        Direction directionExpected = Direction.S;

        roverSpy.turnRobot(directionExpected);

        assertEquals(directionExpected, roverSpy.getDirection());
    }

    @Test
    public void routeReport() throws Exception {
        String stringExpected = String.format("I am %s, my actual position is %s, my actual direction is %s and I found %d obstacles:\n",
                roverSpy.getName(), roverSpy.getPosition().toString(), roverSpy.getDirection().toString(), 0);

        assertEquals(stringExpected, roverSpy.routeReport());
    }
}