package com.marsrover.robot.command;

import org.junit.Test;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit test for CommandFactory class
 *
 * @author Adrián Martín Sánchez
 */
public class CommandFactoryTest {

    private boolean arrayContainsTypeOfCommand(List<Command> array, Class classToCheck) {
        Boolean isInArray = false;

        for (Command command : array) {
            if (command.getClass().equals(classToCheck)) {
                isInArray = true;
                break;
            }
        }

        return isInArray;
    }

    @Test
    public void createCommands_WithNull() throws Exception {
        List<Command> commands = CommandFactory.createCommands(null);

        assertEquals(0, commands.size());
    }

    @Test
    public void createCommands_WithEmptyList() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {});

        assertEquals(0, commands.size());
    }

    @Test
    public void createCommands_Forward() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'f'});

        assertEquals(1, commands.size());
        assertEquals(ForwardCommand.class, commands.get(0).getClass());
    }

    @Test
    public void createCommands_Backward() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'b'});

        assertEquals(1, commands.size());
        assertEquals(BackwardCommand.class, commands.get(0).getClass());
    }

    @Test
    public void createCommands_Left() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'l'});

        assertEquals(1, commands.size());
        assertEquals(LeftCommand.class, commands.get(0).getClass());
    }

    @Test
    public void createCommands_Right() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'r'});

        assertEquals(1, commands.size());
        assertEquals(RightCommand.class, commands.get(0).getClass());
    }

    @Test
    public void createCommands_WithCommandNotRecognize() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'s'});

        assertEquals(0, commands.size());
    }

    @Test
    public void createCommands_MultipleCommands() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'f', 'b', 'l', 'r'});

        assertEquals(4, commands.size());
        assertTrue(arrayContainsTypeOfCommand(commands, ForwardCommand.class));
        assertTrue(arrayContainsTypeOfCommand(commands, BackwardCommand.class));
        assertTrue(arrayContainsTypeOfCommand(commands, LeftCommand.class));
        assertTrue(arrayContainsTypeOfCommand(commands, RightCommand.class));
    }

    @Test
    public void createCommands_MultipleCommandsWithNotRecognize() throws Exception {
        List<Command> commands = CommandFactory.createCommands(new char[] {'f', 'a', 'b', 'c', 'l', 'd', 'r'});

        assertEquals(4, commands.size());
        assertTrue(arrayContainsTypeOfCommand(commands, ForwardCommand.class));
        assertTrue(arrayContainsTypeOfCommand(commands, BackwardCommand.class));
        assertTrue(arrayContainsTypeOfCommand(commands, LeftCommand.class));
        assertTrue(arrayContainsTypeOfCommand(commands, RightCommand.class));
    }
}