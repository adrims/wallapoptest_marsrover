package com.marsrover.robot.command;

import com.marsrover.planet.Planet;
import com.marsrover.position.Coordinates;
import com.marsrover.position.Direction;
import com.marsrover.robot.Rover;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Unit test for RightCommand class
 *
 * @author Adrián Martín Sánchez
 */
public class RightCommandTest {

    Rover roverSpy;
    RightCommand rightCommand = new RightCommand();

    private static final Integer sizeX              = 2;
    private static final Integer sizeY              = 2;
    private static final Integer coordinateRoverX   = 0;
    private static final Integer coordinateRoverY   = 0;
    private static final Direction direction        = Direction.N;

    @Before
    public void setup() throws Exception {
        Planet planet = new Planet.Builder()
                .sizeX(sizeX)
                .sizeY(sizeY)
                .build();

        roverSpy = Mockito.spy(new Rover.Builder()
                .position(new Coordinates(coordinateRoverX, coordinateRoverY))
                .direction(direction)
                .planet(planet)
                .build());
    }

    @Test
    public void run() throws Exception {
        Mockito.doNothing().when(roverSpy).turnRight();

        rightCommand.run(roverSpy);

        Mockito.verify(roverSpy, Mockito.times(1)).turnRight();
    }

}