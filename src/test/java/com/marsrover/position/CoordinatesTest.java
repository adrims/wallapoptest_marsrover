package com.marsrover.position;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for Coordinates class
 *
 * @author Adrián Martín Sánchez
 */

public class CoordinatesTest {

    private Coordinates coordinates;

    private final static Integer coordinateX = 10;
    private final static Integer coordinateY = 15;

    @Before
    public void setup(){
        coordinates = new Coordinates(coordinateX, coordinateY);
    }

    @Test
    public void coordinates_WithNotNumberFormatArguments() throws Exception {
        try {
            Coordinates coordinates = new Coordinates(Integer.parseInt("a"), Integer.parseInt("b"));
            assertTrue("No exception raised", false);
        }
        catch (Exception exception){
            assertTrue(exception.getMessage(), true);
        }
    }

    @Test
    public void equals() throws Exception {
        Coordinates coordinates2 = new Coordinates(coordinateX, coordinateY);

        assertEquals(coordinates, coordinates2);
    }

    @Test
    public void notEquals() throws Exception {
        Coordinates coordinates2 = new Coordinates(coordinateX, coordinateY + 1);

        assertNotEquals(coordinates, coordinates2);
    }

    @Test
    public void toStringTest() throws Exception {
        String stringExpected = "{10, 15}";

        assertEquals(stringExpected, coordinates.toString());
    }

}