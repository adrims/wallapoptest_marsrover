package com.marsrover.planet;

import com.marsrover.position.Coordinates;
import java.util.Objects;

/**
 * Obstacle class
 *
 * @author Adrián Martín Sánchez
 */
public class Obstacle {

    private Coordinates position;

    public Obstacle(Coordinates position) {
        if (position != null) {
            this.position = position;
        }
        else {
            throw new IllegalArgumentException("Obstacle can not be instantiated with position null");
        }
    }

    public Coordinates getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Obstacle obstacle = (Obstacle) o;

        return (position.equals(obstacle.position));
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }


}
