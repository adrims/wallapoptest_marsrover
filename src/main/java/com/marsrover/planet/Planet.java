package com.marsrover.planet;

import com.marsrover.position.Coordinates;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Planet class
 *
 * Class for create the planet where the robot will move
 *
 * @author Adrián Martín Sánchez
 */
public class Planet {

    private String name;
    private Integer sizeX;
    private Integer sizeY;
    private Set<Obstacle> obstacles;
    private Integer [][] map;

    private Planet(Builder builder) {
        if (builder.sizeX != null && builder.sizeY != null &&
                builder.sizeX > 0 && builder.sizeY > 0) {
            this.name       = builder.name;
            this.sizeX      = builder.sizeX;
            this.sizeY      = builder.sizeY;
            this.obstacles  = builder.obstacles;

            initialize();
        }
        else {
            throw new IllegalArgumentException("Planet can not be instantiated with this size");
        }
    }

    public Integer[][] getMap() {
        return map;
    }

    private boolean isWithinPlanetSize(Coordinates position) {
        return (sizeX > position.getCoordinateX() && position.getCoordinateX() >= 0 &&
                sizeY > position.getCoordinateY() && position.getCoordinateY() >= 0);
    }

    private void createObstacles(Set<Obstacle> obstacles) {
        if (obstacles != null && obstacles.size() > 0) {
            obstacles.forEach(obstacle -> fillSquare(calculateNextPosition(obstacle.getPosition()), 1));
        }
    }

    private void fillSquare(Coordinates position, Integer value) {
        map[position.getCoordinateY()][position.getCoordinateX()] = value;
    }

    /**
     * This method calculates a new coordinate if the coordinate is out off planet limits
     *
     * @param coordinate Coordinate to calculate
     * @param size Planet size for calculate limits
     *
     * @return A coordinate within the size of the planet
     */
    private Integer calculateWrapCoordinate(Integer coordinate, Integer size) {
        Integer wrapCoordinate = coordinate % size;

        if (wrapCoordinate < 0) {
            wrapCoordinate = size + wrapCoordinate;
        }

        return wrapCoordinate;
    }

    /**
     * Initialize planet with map sizeX X sizeY and put obstacles into the map
     */
    public void initialize() {
        map = new Integer[sizeY][sizeX];

        for(Integer[] row: map){
            Arrays.fill(row, 0);
        }

        createObstacles(obstacles);
    }

    /**
     * Check if position is already occupied
     *
     * @param position Position to check
     *
     * @return true if position is available, false otherwise
     */
    public boolean isPositionAvailable(Coordinates position) {
        return (map[position.getCoordinateY()][position.getCoordinateX()] == 0);
    }

    /**
     * Calculates next position if it is necessary
     *
     * @param position Position to be calculated
     *
     * @return position if does not need to be wrapped, wrapped position otherwise
     */
    public Coordinates calculateNextPosition(Coordinates position) {
        Integer coordinateX = position.getCoordinateX();
        Integer coordinateY = position.getCoordinateY();

        if (!isWithinPlanetSize(position)) {
            coordinateX = calculateWrapCoordinate(position.getCoordinateX(), sizeX);
            coordinateY = calculateWrapCoordinate(position.getCoordinateY(), sizeY);
        }

        return (new Coordinates(coordinateX, coordinateY));
    }

    public static class Builder {
        private String name;
        private Integer sizeX;
        private Integer sizeY;
        private Set<Obstacle> obstacles;

        public Planet.Builder name(String name) {
            this.name = name;
            return this;
        }

        public Planet.Builder sizeX(Integer sizeX) {
            this.sizeX = sizeX;
            return this;
        }

        public Planet.Builder sizeY(Integer sizeY) {
            this.sizeY = sizeY;
            return this;
        }

        public Planet.Builder obstacles(Set<Obstacle> obstacles) {
            this.obstacles = new HashSet<>(obstacles);
            return this;
        }

        public Planet build() {
            return new Planet(this);
        }
    }
}