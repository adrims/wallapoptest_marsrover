package com.marsrover.robot;

import com.marsrover.position.Coordinates;
import com.marsrover.position.Direction;
import java.util.HashMap;
import java.util.Map;

/**
 * Rover class
 *
 * @author Adrián Martín Sánchez
 */
public class Rover extends Robot {

    private Integer squaresToMove = 1;
    private Integer degreesToMove = 90;

    private static Map<Direction, Integer> directionToDegreesMapper = new HashMap<Direction, Integer>() {{
        put(Direction.W, 0);
        put(Direction.N, 90);
        put(Direction.E, 180);
        put(Direction.S, 270);
    }};

    private Rover(Builder builder) {
        super(builder);
    }

    private void calculateMove(Integer squaresToMove) {
        Integer coordinateToMoveX = position.getCoordinateX();
        Integer coordinateToMoveY = position.getCoordinateY();

        switch (direction) {
            case N: coordinateToMoveY += squaresToMove;
                break;
            case S: coordinateToMoveY -= squaresToMove;
                break;
            case E: coordinateToMoveX += squaresToMove;
                break;
            case W: coordinateToMoveX -= squaresToMove;
                break;
            default:
                break;
        }

        moveRobot(new Coordinates(coordinateToMoveX, coordinateToMoveY));
    }

    private void calculateTurn(Integer degreesToMove) {
        Integer degrees = directionToDegreesMapper.get(direction);

        degrees = (degrees + degreesToMove) % 360;

        if (degrees < 0) {
            degrees += 360;
        }

        turnRobot(getDirectionByDegrees(degrees));
    }

    private Direction getDirectionByDegrees(Integer degrees) {
        return directionToDegreesMapper.entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals(degrees))
                .map(Map.Entry::getKey)
                .findFirst()
                .get();
    }

    @Override
    public void moveForward() {
        calculateMove(squaresToMove);
    }

    @Override
    public void moveBackward() {
        calculateMove(-squaresToMove);
    }

    @Override
    public void turnLeft() {
        calculateTurn(-degreesToMove);
    }

    @Override
    public void turnRight() {
        calculateTurn(degreesToMove);
    }

    public static class Builder extends AbstractBuilder<Rover> {

        @Override
        public Rover build() {
            return new Rover(this);
        }
    }
}