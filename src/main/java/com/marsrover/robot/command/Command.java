package com.marsrover.robot.command;

import com.marsrover.robot.Robot;

/**
 * Command interface
 *
 * @author Adrián Martín Sánchez
 */
public interface Command {

    /**
     * Run the command and make the robot move
     * @param robot Robot to be moved
     */
    void run(Robot robot) throws CommandNotCompatibleException;
}
