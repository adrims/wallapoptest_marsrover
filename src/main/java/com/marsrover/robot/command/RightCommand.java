package com.marsrover.robot.command;

import com.marsrover.robot.Robot;

/**
 * RightCommand class
 *
 * @author Adrián Martín Sánchez
 */
public class RightCommand implements Command {

    @Override
    public void run(Robot robot) {
        robot.turnRight();
    }
}
