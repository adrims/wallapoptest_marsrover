package com.marsrover.robot.command;

import com.marsrover.robot.Robot;

/**
 * BackwardCommand class
 *
 * @author Adrián Martín Sánchez
 */
public class BackwardCommand implements Command {

    @Override
    public void run(Robot robot) {
        robot.moveBackward();
    }
}
