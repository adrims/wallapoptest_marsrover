package com.marsrover.robot.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CommandFactory class
 *
 * Factory for create Command list from char array
 *
 * @author Adrián Martín Sánchez
 */
public class CommandFactory {

    private static Map<Character, Command> characterToCommandMapper = new HashMap<Character, Command>() {{
        put('f', new ForwardCommand());
        put('b', new BackwardCommand());
        put('l', new LeftCommand());
        put('r', new RightCommand());
    }};

    /**
     * Create a list with all accepted commands passed in inputCommands
     *
     * @return Command list
     */
    public static List<Command> createCommands(char [] inputCommands) {
        List<Command> commandList = new ArrayList<>();

        if (inputCommands != null && inputCommands.length > 0) {
            for (char ic : inputCommands) {
                if (characterToCommandMapper.containsKey(ic)) {
                    commandList.add(characterToCommandMapper.get(ic));
                }
            }
        }

        return commandList;
    }
}
