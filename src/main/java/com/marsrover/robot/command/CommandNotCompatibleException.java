package com.marsrover.robot.command;

/**
 * CommandNotCompatibleException class
 *
 * Exception for control not compatible commands
 *
 * @author Adrián Martín Sánchez
 */
public class CommandNotCompatibleException extends Exception {

    public CommandNotCompatibleException() {
        super();
    }

    public CommandNotCompatibleException(String message) {
        super(message);
    }

    public CommandNotCompatibleException(Throwable cause) {
        super(cause);
    }

    public CommandNotCompatibleException(String message, Throwable cause) {
        super(message, cause);
    }
}
