package com.marsrover.robot.command;

import com.marsrover.robot.Robot;

/**
 * LeftCommand class
 *
 * @author Adrián Martín Sánchez
 */
public class LeftCommand implements Command {

    @Override
    public void run(Robot robot) {
        robot.turnLeft();
    }
}
