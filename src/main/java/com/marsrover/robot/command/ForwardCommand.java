package com.marsrover.robot.command;

import com.marsrover.robot.Robot;

/**
 * ForwardCommand class
 *
 * @author Adrián Martín Sánchez
 */
public class ForwardCommand implements Command {

    @Override
    public void run(Robot robot) {
        robot.moveForward();
    }
}
