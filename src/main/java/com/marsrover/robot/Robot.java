package com.marsrover.robot;

import com.marsrover.MarsRoverApplication;
import com.marsrover.planet.Obstacle;
import com.marsrover.planet.Planet;
import com.marsrover.position.Coordinates;
import com.marsrover.robot.command.Command;
import com.marsrover.position.Direction;
import com.marsrover.robot.command.CommandFactory;
import com.marsrover.robot.command.CommandNotCompatibleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Robot abstract class
 *
 * Abstract class for basic movements that a robot can do
 *
 * @author Adrián Martín Sánchez
 */
public abstract class Robot {

    protected String name;
    protected Coordinates position;
    protected Direction direction;
    protected char [] inputCommands;
    protected Planet planet;
    protected Set<Obstacle> obstaclesFound;

    protected static final Logger logger = LoggerFactory.getLogger(MarsRoverApplication.class);


    protected Robot(AbstractBuilder abstractBuilder) {
        if (abstractBuilder.position != null && abstractBuilder.direction != null
                && abstractBuilder.planet != null) {
            this.name           = abstractBuilder.name;
            this.direction      = abstractBuilder.direction;
            this.inputCommands  = abstractBuilder.inputCommands;
            this.planet         = abstractBuilder.planet;
            this.obstaclesFound = new HashSet<>();

            if (this.planet.isPositionAvailable(this.planet.calculateNextPosition(abstractBuilder.position))) {
                this.position = this.planet.calculateNextPosition(abstractBuilder.position);
            }
            else {
                throw new IllegalArgumentException("Robot can not land in this position, it is occupied by an obstacle");
            }
        }
        else {
            throw new IllegalArgumentException("Robot can not be instantiated with this arguments");
        }
    }

    public String getName() {
        return name;
    }

    public Coordinates getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    public char[] getInputCommands() {
        return inputCommands;
    }

    public void setInputCommands(char[] inputCommands) {
        this.inputCommands = inputCommands;
    }

    /**
     * This method executes all the commands that the robot has,
     * is the same for all robots
     * */
    public void runAllCommands() {
        List<Command> commands = CommandFactory.createCommands(inputCommands);

        obstaclesFound.clear();

        if (commands != null && commands.size() > 0) {
            logger.info(String.format("I am %s, starting route from position %s and direction %s with following commands %s\n",
                    this.name, this.position.toString(), direction.toString(), Arrays.toString(inputCommands)));

            commands.forEach(command -> {
                try {
                    command.run(this);
                }
                catch (CommandNotCompatibleException commandNotCompatibleException) {
                    logger.warn(String.format("%s is not compatible with robot %s", command.getClass().getSimpleName(), this.name));
                }
            });

            logger.info(routeReport());
        }
        else {
            logger.info("No commands to execute");
        }
    }

    /**
     * This method checks if the robot can move to the position
     *
     * @param position Position where the robot tries to move
     *
     * @return true if the position is available, false other wise
     */
    protected boolean isPossibleToMove(Coordinates position) {
        return planet.isPositionAvailable(position);
    }

    /**
     * This methods moves the robot to the position if it is possible
     *
     * @param position Position where the robot will move
     */
    protected void moveRobot(Coordinates position) {
        Coordinates nextPosition = planet.calculateNextPosition(position);

        if (isPossibleToMove(nextPosition)) {
            this.position = nextPosition;
        }
        else {
            obstaclesFound.add(new Obstacle(nextPosition));
        }
    }

    /**
     * This method turns the robot to the direction
     *
     * @param direction Direction to where the robot will point
     */
    protected void turnRobot(Direction direction) {
        this.direction = direction;
    }

    /**
     * This method return a report for robot route with the actual position, direction and obstacles found
     *
     * @return Route report
     */
    protected String routeReport() {
        String routeReport = String.format("I am %s, my actual position is %s, my actual direction is %s and I found %d obstacles:\n",
                this.name, this.position.toString(), direction.toString(), obstaclesFound.size());

        Integer iterator = 1;
        for(Obstacle obstacle : obstaclesFound) {
            routeReport += String.format("Obstacle %d in %s\n", iterator, obstacle.getPosition().toString());
            iterator++;
        }

        return routeReport;
    }

    public abstract void moveForward();
    public abstract void moveBackward();
    public abstract void turnLeft();
    public abstract void turnRight();


    public static abstract class AbstractBuilder<T> {

        protected String name;
        protected Coordinates position;
        protected Direction direction;
        protected char [] inputCommands;
        protected Planet planet;

        public Robot.AbstractBuilder<T> name(String name) {
            this.name = name;
            return this;
        }

        public Robot.AbstractBuilder<T> position(Coordinates position) {
            this.position = position;
            return this;
        }

        public Robot.AbstractBuilder<T> direction(Direction direction) {
            this.direction = direction;
            return this;
        }

        public Robot.AbstractBuilder<T> inputCommands(char [] inputCommands) {
            this.inputCommands = inputCommands;
            return this;
        }

        public Robot.AbstractBuilder<T> planet(Planet planet) {
            this.planet = planet;
            return this;
        }

        public abstract T build();
    }
}