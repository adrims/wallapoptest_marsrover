package com.marsrover;

import com.marsrover.planet.Obstacle;
import com.marsrover.planet.Planet;
import com.marsrover.position.Coordinates;
import com.marsrover.position.Direction;
import com.marsrover.robot.Rover;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

@SpringBootApplication
public class MarsRoverApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(MarsRoverApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MarsRoverApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		try {
			Scanner reader = new Scanner(System.in);

			System.out.println("Insert planet name:");
			String planetName = reader.next();
			System.out.println("Insert horizontal planet size:");
			int sizeX = reader.nextInt();
			System.out.println("Insert vertical planet size:");
			int sizeY = reader.nextInt();


			System.out.println("Insert number of obstacles:");
			int obstaclesNumber = reader.nextInt();

			int coordinateX;
			int coordinateY;
			Set<Obstacle> obstacles = new HashSet<>();

			for (int i = 1; i <= obstaclesNumber; i++) {
				System.out.println("Obstacle " + i);
				System.out.println("Insert coordinate x:");
				coordinateX = reader.nextInt();
				System.out.println("Insert coordinate y:");
				coordinateY = reader.nextInt();
				obstacles.add(new Obstacle(new Coordinates(coordinateX, coordinateY)));
			}

			Planet planet = new Planet.Builder()
					.name(planetName)
					.sizeX(sizeX)
					.sizeY(sizeY)
					.obstacles(obstacles)
					.build();

			System.out.println("Insert robot name:");
			String robotName = reader.next();
			System.out.println("Insert horizontal initial rover position:");
			int coordinateRoverX = reader.nextInt();
			System.out.println("Insert vertical initial rover position:");
			int coordinateRoverY = reader.nextInt();
			System.out.println("Insert initial rover direction (N,S,E,W):");
			Direction roverDirection = Direction.valueOf(reader.next().toUpperCase()); //n = north, e = east, w = west, s = south

			System.out.println("Insert command array (f,b,l,r) or N to exit:");
			String inputCommands = reader.next();

			Rover rover = new Rover.Builder()
					.name(robotName)
					.position(new Coordinates(coordinateRoverX, coordinateRoverY))
					.direction(roverDirection)
					.planet(planet)
					.inputCommands(inputCommands.toLowerCase().toCharArray())
					.build();

			while (!inputCommands.toUpperCase().equals("N")) {
				rover.setInputCommands(inputCommands.toLowerCase().toCharArray());
				rover.runAllCommands();

				System.out.println("Insert command array (f,b,l,r) or N to exit:");
				inputCommands = reader.next();
			}

			System.exit(0);
		}
		catch (Exception exception) {
			logger.error(exception.getMessage());
			System.exit(1);
		}
	}
}
