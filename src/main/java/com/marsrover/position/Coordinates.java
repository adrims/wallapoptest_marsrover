package com.marsrover.position;

import java.util.Objects;

/**
 * Coordinates class
 *
 * @author Adrián Martín Sánchez
 */
public class Coordinates {

    private Integer coordinateX;
    private Integer coordinateY;

    public Coordinates(int coordinateX, int coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public Integer getCoordinateX() {
        return coordinateX;
    }

    public Integer getCoordinateY() {
        return coordinateY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Coordinates coordinates = (Coordinates) o;

        return (coordinateX.equals(coordinates.coordinateX) && coordinateY.equals(coordinates.coordinateY));
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinateX, coordinateY);
    }

    @Override
    public String toString() {
        return String.format("{%d, %d}", coordinateX, coordinateY);
    }

}
