package com.marsrover.position;

/**
 * This enums represents the cardinal direction an object is pointing to
 *
 * @author Adrián Martín Sánchez
 */
public enum  Direction {
    N, S, E, W
}
