package com.marsrover.robot;

import com.marsrover.planet.Obstacle;
import com.marsrover.planet.Planet;
import com.marsrover.position.Coordinates;
import com.marsrover.position.Direction;
import org.junit.Before;
import org.junit.Test;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Integration test for Robot class
 *
 * @author Adrián Martín Sánchez
 */
public class RobotIT {

    private Planet planet;
    private Rover rover;
    private Set<Obstacle> obstacles = new HashSet<>();

    private static final char [] inputCommands      = new String("frflfflf").toCharArray();
    private static final String planetName          = "Mars";
    private static final String robotName           = "Rover";
    private static final Integer sizeX              = 4;
    private static final Integer sizeY              = 4;
    private static final Integer coordinateX        = 0;
    private static final Integer coordinateY        = 1;
    private static final Integer coordinateRoverX   = -4;
    private static final Integer coordinateRoverY   = -4;
    private static final Direction direction        = Direction.N;

    @Before
    public void setup() throws Exception {
        obstacles.add(new Obstacle(new Coordinates(coordinateX, coordinateY)));

        planet = new Planet.Builder()
                .name(planetName)
                .sizeX(sizeX)
                .sizeY(sizeY)
                .obstacles(obstacles)
                .build();

        rover = new Rover.Builder()
                .name(robotName)
                .position(new Coordinates(coordinateRoverX, coordinateRoverY))
                .direction(direction)
                .planet(planet)
                .inputCommands(inputCommands)
                .build();
    }

    @Test
    public void runAllCommands() throws Exception {
        Coordinates positionExpected    = new Coordinates(0,2);
        Direction directionExpected     = Direction.W;

        rover.runAllCommands();

        assertEquals(positionExpected, rover.getPosition());
        assertEquals(directionExpected, rover.getDirection());
    }
}
